<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [UserController::class, 'index']);
Route::get('locale/{locale}', function ($locale) {
    // dd($locale);
    app()->setLocale($locale);
    session()->put('locale', $locale);
    return redirect()->back();
})->name('locale');

Route::get('/posts', [PostsController::class, 'index']);
Route::get('/posts-add', [PostsController::class, 'create'])->middleware('auth');
Route::post('/creatingposts', [PostsController::class, 'store']);


Route::get('/signin', [AuthController::class, 'index'])->name('login')->middleware('guest');
Route::post('/loging-in', [AuthController::class, 'signin'])->middleware('guest');
Route::get('/signout', [AuthController::class, 'signout'])->middleware('auth');
