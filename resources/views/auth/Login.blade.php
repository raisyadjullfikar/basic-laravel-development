@extends('layouts.layout')

@section('content')

    <div class="container-lg mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Login</h3>
                    </div>
                    @if ($message = Session::get('failed'))
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{ $message }}</li>
                            </ul>
                        </div>  
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('status'))
                        <div class="alert alert-danger" id="statusAlert" role="alert">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="card-body">
                        {{ Session::get('message') }}
                        <form action="/loging-in" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email">
                            </div>
                            @error('email')
                                <small>{{ $message }}</small>
                            @enderror
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            @error('password')
                                <small>{{ $message }}</small>
                            @enderror
                            <div class="mb-3">
                                <p>Don't have account ? Register <a href="/reg">here</a></p>
                            </div>
                            <div class="d-grid">
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var statusAlert = document.getElementById('statusAlert');

            statusAlert.style.opacity = 1;

            setTimeout(function() {
                fadeOut(statusAlert);
            }, 2000);
        });

        function fadeOut(element) {
            var opacity = 1;
            var timer = setInterval(function() {
                if (opacity <= 0.1) {
                    clearInterval(timer);
                    element.style.display = 'none';
                }
                element.style.opacity = opacity;
                opacity -= 0.1;
            }, 50);
        }
    </script>

@endsection
