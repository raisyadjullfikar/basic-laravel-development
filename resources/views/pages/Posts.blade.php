@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="mt-3">
            @if (Session::has('guest'))
                <div class="alert alert-danger" id="statusAlert" role="alert">
                    {{ Session::get('guest') }}
                </div>
            @elseif (Session::has('addpost'))
                <div class="alert alert-success" id="statusAlert" role="alert">
                    {{ Session::get('addpost') }}
                </div>
            @elseif (Session::has('logout'))
                <div class="alert alert-warning" id="statusAlert" role="alert">
                    {{ Session::get('logout') }}
                </div>
            @endif
        </div>
        <div class="mt-3">
            <a class="btn btn-success" href="/posts-add">
                Add Posts
            </a>
        </div>
        <table class="table table-dark table-hover mt-3">
            <thead>
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Posts</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($Posts as $p)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $p->posts }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
