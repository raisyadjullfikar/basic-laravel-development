@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="mt-5 col-8 m-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="/creatingposts" method="POST">
                @csrf
                <div class="mb-3 border-warning">
                    <label for="posts">New Posts</label>
                    <input type="text" class="form-control mt-2" name="posts" id="posts"
                        value="{{ old('posts') }}">
                </div>
                @error('email')
                    <small>{{ $message }}</small>
                @enderror

                <div class="mb-3">
                    <button class="btn btn-success" type="submit">Save Data</button>
                </div>
            </form>
        </div>
    </div>
@endsection
