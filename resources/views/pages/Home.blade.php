@extends('layouts.layout')

@section('content')

<div class="container">
    <div class="mt-3">
        @if (Auth()->user())
            <span>Hello, {{ Auth()->user()->name }} you was created on {{ Auth()->user()->created_at->diffForHumans() }}</span>
            <br>
            <label for="testing">{{ __('nama') }} : </label>
            <br>
            <span>Nama kamu {{ Auth()->user()->name }}</span>
            <br>
            <label for="testing">{{ __('email') }} : </label>
            <br>
            <span>Email kamu {{ Auth()->user()->email }}</span>
            <br>
            <label for="testing">{{ __('created_at') }} : </label>
            <br>
            <span>{{ Auth()->user()->created_at->diffForHumans() }}</span>
            <br>
        @else
            <span>You are guest for now</span>
        @endif
    </div>
</div>

@endsection