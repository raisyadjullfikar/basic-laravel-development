<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Facades\View as FacadesView;
use Illuminate\View\Factory as View;

class UserController extends Controller
{
    private $request;
    private $view;

    public function __construct(Request $request, View $view) {
        $this->request = $request;
        $this->view = $view;
    }

    public function index(){
        // $name = $this->request->get('name');
        // return $this->view->make('welcome', ['name' => $name]);
        return $this->view->make('pages.Home', ['title' => 'Homepage']);
        // return app('view')->make('welcome');
        // return view('pages.Home', ['title' => 'Homepage']);
    }
}
