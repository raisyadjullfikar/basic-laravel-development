<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class PostsController extends Controller
{
    public function index(){
        $data = User::get();
        $dataPosts = Post::get();

        return view('pages.Posts', ['data' => $data, 'Posts' => $dataPosts, 'title' => 'Index Page']);
    }

    public function create(){
        return view('pages.AddPost', ['title' => 'Add Posts']);
    }

    public function store(Request $request){
        if(!Auth::check()) return redirect('/')->with('guest', 'You are guest, cant create the new posts');
        else {
            $validator = Validator::make($request->all(), [
                'posts' => 'required|max:190|min:1'
            ]);

            if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

            $data['posts'] = $request->posts;

            $post = Post::create($data);
            if($post) Session::flash('addpost', "Success add posts");

            return redirect('/');
        }
    }
}
