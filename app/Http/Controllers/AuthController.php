<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index() {
        return view('auth.Login', ['title' => 'Login Page']);
    }
    public function signin(Request $request) {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        return Auth::attempt($data) ? redirect('/') : redirect('/signin')->with('failed', 'Email atau password salah');
    }

    public function signout() {
        Auth::logout();
        return redirect('/')->with('logout', 'You was logout');
    }
}
